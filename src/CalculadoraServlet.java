

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CalculadoraServlet
 */
@WebServlet("/CalculadoraServlet")
public class CalculadoraServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalculadoraServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("estic al servlet");
		String num1 = request.getParameter("num1");
		String num2 = request.getParameter("num2");
		String operacio = request.getParameter("operacio");

		double resultat = 0;
		
		// suposo que m'arriben els 2 numeros plens
		// faltarien aquestes comprovacions

		switch(isIntegerParseInt(operacio)) {
	    case 1:
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) + ((num2!=null)?isIntegerParseInt(num2):0);
	    	break;
	    case 2:
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) - ((num2!=null)?isIntegerParseInt(num2):0);
	    	break;
	    case 3:
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) * ((num2!=null)?isIntegerParseInt(num2):0);
	    	break;
	    case 4:
	    	if (isIntegerParseInt(num2)==0){
	    		resultat=0;
	    	}
	    	else
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) / isIntegerParseInt(num2);
	        break;
	    default:
	        resultat=0;
		}

		request.setAttribute("resultat", resultat);
		 
		RequestDispatcher view = request.getRequestDispatcher("/CalculadoraAmbServlet.html");
		view.forward(request, response);		 
	}
	public int isIntegerParseInt(String str) {
		int numero=0;
	    try {
	        numero=Integer.parseInt(str);        
	    } catch (NumberFormatException nfe) {}
	    return numero;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
