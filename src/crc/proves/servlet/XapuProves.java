package crc.proves.servlet;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.prefs.Preferences;


public class XapuProves {
	
	public static void main(String[] args){
		System.out.println(Locale.getDefault());
		System.out.println("------");
		System.out.println("numberformagt : "+ NumberFormat.getInstance().toString());
		//System.out.println("preferences.getLocale() : "+ Preferences. getLocale());
		
		
		Locale myLocale = new Locale("ca","ES");
		System.out.println(myLocale.getDefault());
		System.out.println("------");
		System.out.println("------");
		System.out.println("My country (ISO): " + myLocale.getCountry());
		System.out.println("My country name: " + myLocale.getDisplayCountry());
		 
		System.out.println("My language (code): " + myLocale.getLanguage());
		System.out.println("My language (name): " + myLocale.getDisplayLanguage());
		 
		System.out.println(myLocale.getDisplayName());
		
		System.out.println("------");
		System.out.println("------");
		
		Locale deLocale = Locale.GERMANY;
		// Default system Locale is en_US for this method call.
		String defaultLanguage = deLocale.getDisplayLanguage();	
		System.out.println(defaultLanguage);  // -> aleman en espa�ol
		
		// Target de_DE is used as an explicit target language.
		String targetLanguage = deLocale.getDisplayLanguage(deLocale);
		System.out.println(targetLanguage);  // -> deutsch en aleman
		
		System.out.println("------");
		System.out.println("------");
		
		Locale aLocale = Locale.JAPAN;
		System.out.println("Locale: " + aLocale); System.out.println("ISO 2 letter: " 
		   + aLocale.getLanguage()); 
		System.out.println("ISO 3 letter: " + aLocale.getISO3Language()); 
		aLocale = Locale.US;
		System.out.println("Locale:" + aLocale);
		System.out.println("ISO 2 letter: " + aLocale.getLanguage()); 
		System.out.println("ISO 3 letter: " + aLocale.getISO3Language()); 

		System.out.println("------");
		System.out.println("------");
		
		Locale[] locales = { new Locale("en", "US"), new Locale("ja","JP"),
		        new Locale("es", "ES"), new Locale("it", "IT") }; 
		for (int x=0; x< locales.length; ++x) { 
		    String displayLanguage = locales[x].getDisplayLanguage(locales[x]); 
		    System.out.println(locales[x].toString() + ": " + displayLanguage); 
		} 

		 
		
		provantQuinsLocales();
	}
	
	private static void provantQuinsLocales(){
	
	/* To find out what locales your Java Runtime Environment (JRE) supports, 
	 * you have to ask each locale-sensitive class. 
	 * Each class that supports multiple locales will implement the method getAvailableLocales(). 
	 * For example,
	 */

		Locale[] localeList = NumberFormat.getAvailableLocales();
		for (Locale locale : localeList) {
			System.out.println("------");
			System.out.println("------");
			System.out.println("default: " + locale.getDefault());
			System.out.println("My country (ISO): " + locale.getCountry());
			System.out.println("My country name: " + locale.getDisplayCountry());
			 
			System.out.println("My language (code): " + locale.getLanguage());
			System.out.println("My language (name): " + locale.getDisplayLanguage());
			 
			System.out.println(locale.getDisplayName());
		}

	 /* The getAvailableLocales() method is implemented in many of the classes in the java.text and 
	  * java.util packages. For example, NumberFormat, DateFormat, Calendar, and BreakIterator provide it.
	  */
	}
}
