import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;


public class AlumnoDao {
	// static initializer - nom�s s'executa a l'inicialitzar la classe
	//                      no per cada objecte que es crea
		/* 
		 * 1er REGISTRAR EL DRIVER
		 */
	static{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int addAlumno(Alumno alumno){
		int filasAfectadas=0;
		try {
			/* 
			 * 2on OBRIR UNA CONEXI� -li passem: la url de la BDD i l'usuari
			 */
			Connection connect = DriverManager
				      .getConnection("jdbc:mysql://localhost/javatardes?"
				          + "user=root");
			/* 
			 * 3er EXECUTAR UNA QUERY
			 */
			Statement statement = connect.createStatement();
			String insertTableSQL = "INSERT INTO alumno"
					+ "(nombre,apellidos,dni) " + "VALUES "
					+ "('" + alumno.getNombre() + "','" + 
						alumno.getApellidos() + "','" + alumno.getDni() + "')";		
			// retorna:
			//   .- el numero de files afectades Data Manipulation Language -DML
			//   .- o 0 si no retorna res
			filasAfectadas=statement.executeUpdate(insertTableSQL);
			
			/* 
			 * 4art EXTREURE/Tractar LES DADES DEL RESULTAT
			 *      en aquest cas ens retorna un integer, pero podria ser un ResultSet
			 *      ResultSet rs = stmt.executeQuery(sql);
			 *
			 *     while(rs.next()){
			 *        //Retrieve by column name
			 *        int id  = rs.getInt("id");
			 *        int age = rs.getInt("age");
			 *        String first = rs.getString("first");
			 *        String last = rs.getString("last");
			 *
			 *        //Display values
			 *        System.out.print("ID: " + id);
			 *        System.out.print(", Age: " + age);
			 *        System.out.print(", First: " + first);
			 *        System.out.println(", Last: " + last);
			 *     
			 *     //STEP 6: Clean-up environment
			 *     rs.close();
			 *     stmt.close();
			 *     conn.close();
			 */

		} catch (SQLException e) {
				e.printStackTrace();
		}
		return filasAfectadas;
	}
	
	public int addAlumnoPreparedStatement(Alumno alumno){
		int filasAfectadas=0;
		try{
			PreparedStatement preparedStatement = null;
			Connection connect = DriverManager
				      .getConnection("jdbc:mysql://localhost/javatardes?"
				          + "user=root");
		
			preparedStatement = connect
		          .prepareStatement("insert into alumno (nombre,apellidos,dni) values (?, ?, ?)");
			
			preparedStatement.setString(1, alumno.getNombre());
			preparedStatement.setString(2, alumno.getApellidos());
			preparedStatement.setString(3, alumno.getDni());
			
			filasAfectadas=preparedStatement.executeUpdate();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		return filasAfectadas;
	}
}
