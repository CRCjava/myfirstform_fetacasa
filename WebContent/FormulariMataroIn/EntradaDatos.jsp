<%@page import="javax.annotation.Resource"%>
<%@page import="java.util.PropertyResourceBundle,java.util.Locale,java.util.ResourceBundle"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>(MULTIIDIOMA) Formulario y validacion</title>
	<link rel="stylesheet" type="text/css" href="form.css">
</head>
<body>
	<form name="idioma" action="EntradaDatos.jsp" method="get">
		<button type="button" name="botoIdioma" id="bt_cat" onclick="assignarIdioma(0);">CAT</button>
		<button type="button" name="botoIdioma" id="bt_esp" onclick="assignarIdioma(1);">ESP</button>
		<button type="button" name="botoIdioma" id="bt_eng" onclick="assignarIdioma(2);">ENG</button>
		<button type="button" name="botoIdioma" id="bt_fra" onclick="assignarIdioma(3);">FRA</button>
		<input type="hidden" name="hdIdioma" id="hdIdioma">	
	</form>
		
	<%
	// create and load default properties
	final String langEtiquetes = "crc.formulari.multiidioma.Etiquetes"; 
	PropertyResourceBundle etiquetes=null;	
	if (request.getParameter("hdIdioma")!=null){
		switch(Integer.parseInt(request.getParameter("hdIdioma"))){
		case 0: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
				break;
		case 1: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("es", "ES"));
				break;
		case 2: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("en", "GB"));
				break;
		case 3: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("fr", "FR"));
				break;
		default: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
				break;
		}
	}else {
		System.out.println("ara estem en el else");
		etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
	}%>
<div class="formu">
	<h1><%=etiquetes.getString("h1") %></h1>
	<form action="RecibirDatos.jsp" method="post" onsubmit="return validaForm()">
	
		<label name="lblNom" id="lblNom"><%=etiquetes.getString("lblNom") %></label>
		<input name="nombre" type="text" id="nombre" class="inputs"/><br>
		
		<label name="lblCognoms" id="lblCognoms"><%=etiquetes.getString("lblCognoms") %></label>
		<input name="apellidos" type="text" id="apellidos" class="inputs"/><br>		
				
		<label name="lblEmpresa" id="lblEmpresa"><%=etiquetes.getString("lblEmpresa") %></label>
		<input name="empre" type="text" id="empre" class="inputs"/><br>
		
		<label name="lblTelf" id="lblTelf"><%=etiquetes.getString("lblTelf") %></label>
		<input name="telf" type="tel" id="telf" class="inputs" /><br>
		
		<label name="lblCorreu" id="lblCorreu"><%=etiquetes.getString("lblCorreu") %></label>
		<input name="correo" type="email" id="correo" class="inputs" /><br>
		
		<label name="lblCiutat" id="lblCiutat"><%=etiquetes.getString("lblCiutat") %></label>
		<input name="ciudad" type="text" id="ciudad" class="inputs"/><br>
		
		<label name="lblComentari" id="lblComentari"><%=etiquetes.getString("lblComentari") %></label>
		<textarea name="comment" id="comment" cols="60" rows="7"></textarea>
		
		<input type="checkbox" value="si" id="privacidad"><%=etiquetes.getString("cbxPrivacitat") %><br>
		
		<button type="submit" name="enviar" id="enviar"><%=etiquetes.getString("btnEnviar") %></button>	
	</form>
</div>
<script src="formulario.js"></script>
</body>
</html>