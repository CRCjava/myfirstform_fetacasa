/**
 * 
 */

function assignarIdioma(idioma){
	switch(idioma){
	case 0: 
		document.getElementById("hdIdioma").value="0";
		break;
	case 1: 
		document.getElementById("hdIdioma").value="1";
        break;
	case 2: 
		document.getElementById("hdIdioma").value="2";
    	break;
	case 3: 
		document.getElementById("hdIdioma").value="3";
    	break;
    default: document.getElementById("hdIdioma").value="0";
	}
	document.forms["idioma"].submit();
}

function validaForm(){
	var nom = document.getElementById("nombre").value;
	var ape = document.getElementById("apellidos").value;
	var empre = document.getElementById("empre").value;
	var ciu = document.getElementById("ciudad").value;
	var telef = document.getElementById("telf").value;
	var email = document.getElementById("mail").value;
	var nota = document.getElementById("comment").value;

	var aceptar=document.getElementById("privacidad");
// checkear conforme

if (!aceptar.checked) {
      
      alert("chequee la politica de privacidad");
      return false;
   }
   

// valida nombre y apellidos
	if (nom == null || nom ==""){
		alert("Nombre esta vacio");
		return false;
	}
	if (ape == null || ape ==""){
		alert("Apellidos esta vacio");
		return false;
	}
	if (empre == null || empre ==""){
		alert("Campo Empresa VACIO");
		return false;
	}
	

// valida el telefono
	if (telef == null || telef == "") {
      alert("Telefono esta vacio");
      return false;
    }
	if ( !(/^\+\d{2,3}\s\d{9}$/.test(telef)) ) {
      alert("telefono incorreto - Ej.+34 900888777");
      return false;
    }

// valida el email correctamente 
	if (email == null || email == "") {
      alert("mail esta vacio");
      return false;
    }

expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

	if ( !expr.test(email) ) {
      alert("mail incorreto");   
      return false;
    }
    
    if (ciu == null || ciu ==""){
		alert("Campo Ciudad esta VACIO");
		return false;
	}

    if (nota == null || nota ==""){
		alert("Escribe un comentario");
		return false;
	}

	else{
		return true;
	}
}