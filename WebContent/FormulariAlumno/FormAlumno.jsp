<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.PropertyResourceBundle,java.util.Locale,java.util.ResourceBundle"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="FormAlumno.css">
</head>
<body>

<%!
    Alumno alumno = null;
    Alumno crearAlumno(String nom, String cognoms, String dni){
    	alumno=new Alumno();
        alumno.setNombre(nom);
        alumno.setApellidos(cognoms);
        alumno.setDni(dni);
        return alumno;
    }
    AlumnoDao alumnoDao = new AlumnoDao();
   
%>

	<form name="idioma" action="FormAlumno.jsp" method="get">
		<button type="button" name="botoIdioma" id="bt_cat" onclick="assignarIdioma(0);">CAT</button>
		<button type="button" name="botoIdioma" id="bt_esp" onclick="assignarIdioma(1);">ESP</button>
		<button type="button" name="botoIdioma" id="bt_eng" onclick="assignarIdioma(2);">ENG</button>
		<button type="button" name="botoIdioma" id="bt_fra" onclick="assignarIdioma(3);">FRA</button>
		<input type="hidden" name="hdIdioma" id="hdIdioma">	
	</form>
	<% // create and load default properties
	final String langEtiquetes = "crc.formulari.multiidioma.EtiqFormAlumno"; 
	PropertyResourceBundle etiquetes=null;	
	if (request.getParameter("hdIdioma")!=null){
		switch(Integer.parseInt(request.getParameter("hdIdioma"))){
		case 0: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
				break;
		case 1: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("es", "ES"));
				break;
		case 2: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("en", "GB"));
				break;
		case 3: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("fr", "FR"));
				break;
		default: etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
				break;
		}
	}else {
		System.out.println("ara estem en el else");
		etiquetes = (PropertyResourceBundle)ResourceBundle.getBundle(langEtiquetes, new Locale("ca", "ES"));
	}%>
<div class="formu">
	<h1><%=etiquetes.getString("h1") %></h1>
	<form action="FormAlumno.jsp" method="post" onsubmit="return validaForm()">
	
		<label name="lblNom" id="lblNom"><%=etiquetes.getString("lblNom") %></label>
		<input name="nombre" type="text" id="nombre" class="inputs"/><br>
		
		<label name="lblCognoms" id="lblCognoms"><%=etiquetes.getString("lblCognoms") %></label>
		<input name="apellidos" type="text" id="apellidos" class="inputs"/><br>		
				
		<label name="lblDni" id="lblDni"><%=etiquetes.getString("lblDni") %></label>
		<input name="dni" type="text" id="dni" class="inputs"/><br>
		
		<input type="hidden" name="hdOk" id="hdOk">
				
		<button type="submit" name="enviar" id="enviar"><%=etiquetes.getString("enviar") %></button>	
	</form>
	<% if (request.getParameter("hdOk")=="ok"){
		alumnoDao.addAlumno(crearAlumno(request.getParameter("nombre"), request.getParameter("apellidos"), 
				request.getParameter("dni")));
	}
	%>
</div>
<script src="FormAlumno.js"></script>

</body>
</html>