<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html> // PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">
		function processarOperacio(operacio){
			document.getElementById("operacio").setAttribute("value", operacio);
			document.forms["myform"].submit();
			
		}
	</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Calculadora JSP de Carme Riu</title>
</head>
<body>
  <%!
  	public int isIntegerParseInt(String str) {
	  int numero=0;
      try {
          numero=Integer.parseInt(str);        
      } catch (NumberFormatException nfe) {}
      return numero;
  	}
  	 String num1="";
     String num2=""; 
     String operacio="";
     int resultat=0;
     %>
  <% num1=request.getParameter("num1");
  	num2=request.getParameter("num2");
  	operacio=request.getParameter("operacio");%>
  	
	<h3>CALCULADORA</h3>
	<!-- action - pagina web on li envio les dades -->
	<form name="myform" action="Calculadora.jsp" method="Get">
		<div id="lblNum1">First Numero: </div>
			<input type="number" name="num1" id="num1" value="<%= num1%>" required><br>
		<div id="lblNum2">Segon Numero: </div>
			<input type="number" name="num2" id="num2"  value="<%= num2%>"><br>
			
		<input type="button" value="+" onclick=processarOperacio("1") name="suma" />
		<input type="button" value="-" onclick=processarOperacio("2") name="resta" />
		<input type="button" value="*" onclick=processarOperacio("3") name="mult" />
		<input type="button" value="/" onclick=processarOperacio("4") name="div" />
		<input type="hidden" name="operacio" id="operacio">
	</form>
  <%
  	if (operacio != null){  		
	  	switch(isIntegerParseInt(operacio)) {
	    case 1:
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) + ((num2!=null)?isIntegerParseInt(num2):0);
	    	break;
	    case 2:
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) - ((num2!=null)?isIntegerParseInt(num2):0);
	    	break;
	    case 3:
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) * ((num2!=null)?isIntegerParseInt(num2):0);
	    	break;
	    case 4:
	    	if ((num2==null) || (isIntegerParseInt(num2)==0)){
	    		resultat=0;
	    	}
	    	else
	    	resultat=((num1!=null)?isIntegerParseInt(num1):0) / isIntegerParseInt(num2);
	        break;
	    default:
	        resultat=0;
		} 
		  	
  	%>
  	<h4> solucio <%= resultat %> </h4>
  
  <%} %>
</body>
</html>